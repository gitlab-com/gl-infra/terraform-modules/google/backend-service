# GitLab.com backend-service Terraform Module

## What is this?

This module is an extraction of the resources managing backend services from existing modules like
[generic-sv-with-group](https://gitlab.com/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group)
and [generic-stor-with-group](https://gitlab.com/gitlab-com/gl-infra/terraform-modules/google/generic-stor-with-group).

This module was made to be used in the [db-provisioning](https://gitlab.com/gitlab-com/gl-infra/ansible-workloads/db-provisioning) project.
The aforementioned modules are not making use of this module yet as it would involve fiddling with Terraform states to avoid breaking things.
Should there be an effort to make such move, it's easy to use this module as such:

```
module "backend_service" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/backend-service.git"

  environment            = var.environment
  name                   = var.name
  region                 = var.region
  instance_self_links    = google_compute_instance.default.*.self_link
  instance_zones         = google_compute_instance.default.*.zone
  service_path           = var.service_path
  service_port           = var.service_port
  health_check_port      = var.health_check_port
  health_check           = var.health_check
  backend_service_type   = var.backend_service_type
  enable_iap             = var.enable_iap
  create_backend_service = var.create_backend_service
  backend_protocol       = var.backend_protocol
  timeout_sec            = var.timeout_sec
  oauth2_client_id       = var.oauth2_client_id
  oauth2_client_secret   = var.oauth2_client_secret
}
```

## What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale our infrastructure. It's provider agnostic so it works great for our use case. You're encouraged to read the documentation as it's very well written. Reading [Terraform: Up and Running](https://www.amazon.com/Terraform-Running-Writing-Infrastructure-Code-ebook/dp/B06XKHGJHP) is also recommended.

## Contributing

Please see [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
