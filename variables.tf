variable "environment" {
  type        = string
  description = "The environment name"
}

variable "name" {
  type        = string
  description = "The backend service name"
}

variable "region" {
  type        = string
  description = "The target region"
}

variable "instance_self_links" {
  default = []
}

variable "instance_zones" {
  default = []
}

variable "service_path" {
  default = "/"
}

variable "service_port" {
  type    = string
  default = "80"
}

variable "health_check_port" {
  default = "use service port"
}

variable "health_check" {
  default = "http"
}

variable "backend_service_type" {
  default     = "regular"
  description = "type of backend service, either normal or regional"
}

variable "enable_iap" {
  default = false
}

variable "create_backend_service" {
  default = true
}

variable "backend_protocol" {
  default = "HTTP"
}

variable "timeout_sec" {
  default = 30
}

variable "oauth2_client_id" {
  default = ""
}

variable "oauth2_client_secret" {
  default = ""
}
