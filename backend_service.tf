resource "google_compute_health_check" "tcp" {
  name = format("%v-%v-tcp", var.environment, var.name)

  tcp_health_check {
    port = var.health_check_port == "use service port" ? var.service_port : var.health_check_port
  }
}

resource "google_compute_health_check" "http" {
  name = format("%v-%v-http", var.environment, var.name)

  http_health_check {
    port         = var.health_check_port == "use service port" ? var.service_port : var.health_check_port
    request_path = var.service_path
  }
}

# Add one instance group per zone
# and only select the appropriate instances
# for each one

resource "google_compute_instance_group" "default" {
  count = length(data.google_compute_zones.available.names)
  name = format(
    "%v-%v-%v",
    var.environment,
    var.name,
    data.google_compute_zones.available.names[count.index],
  )
  description = "Instance group for monitoring VM."
  zone        = data.google_compute_zones.available.names[count.index]

  named_port {
    name = var.name
    port = var.service_port
  }

  named_port {
    name = "http"
    port = "80"
  }

  named_port {
    name = "https"
    port = "443"
  }

  # This filters the full set of instances to only ones for the appropriate zone.
  instances = matchkeys(
    var.instance_self_links,
    var.instance_zones,
    [data.google_compute_zones.available.names[count.index]],
  )
}

resource "google_compute_region_backend_service" "default" {
  count    = var.backend_service_type == "regional" && var.create_backend_service ? 1 : 0
  name     = format("%v-%v-regional", var.environment, var.name)
  protocol = "TCP"

  backend {
    group = google_compute_instance_group.default[0].self_link
  }

  backend {
    group = google_compute_instance_group.default[1].self_link
  }

  backend {
    group = google_compute_instance_group.default[2].self_link
  }

  health_checks = [var.health_check == "http" ? google_compute_health_check.http.self_link : google_compute_health_check.tcp.self_link]
}

resource "google_compute_backend_service" "default" {
  count       = var.backend_service_type == "regional" || var.enable_iap || false == var.create_backend_service ? 0 : 1
  name        = format("%v-%v", var.environment, var.name)
  protocol    = var.backend_protocol
  port_name   = var.name
  timeout_sec = var.timeout_sec

  backend {
    balancing_mode = "UTILIZATION"
    group          = google_compute_instance_group.default[0].self_link
  }

  backend {
    balancing_mode = "UTILIZATION"
    group          = google_compute_instance_group.default[1].self_link
  }

  backend {
    balancing_mode = "UTILIZATION"
    group          = google_compute_instance_group.default[2].self_link
  }

  health_checks = [var.health_check == "http" ? google_compute_health_check.http.self_link : google_compute_health_check.tcp.self_link]
}

resource "google_compute_backend_service" "iap" {
  count       = var.backend_service_type != "regional" && var.enable_iap && var.create_backend_service ? 1 : 0
  name        = format("%v-%v", var.environment, var.name)
  protocol    = var.backend_protocol
  port_name   = var.name
  timeout_sec = var.timeout_sec

  backend {
    group = google_compute_instance_group.default[0].self_link
  }

  backend {
    group = google_compute_instance_group.default[1].self_link
  }

  backend {
    group = google_compute_instance_group.default[2].self_link
  }

  health_checks = [var.health_check == "http" ? google_compute_health_check.http.self_link : google_compute_health_check.tcp.self_link]

  iap {
    oauth2_client_secret = var.oauth2_client_secret
    oauth2_client_id     = var.oauth2_client_id
  }
}
