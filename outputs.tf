output "http_health_check_self_link" {
  value = google_compute_health_check.http.self_link
}

output "tcp_health_check_self_link" {
  value = google_compute_health_check.tcp.self_link
}

output "instance_groups_self_link" {
  value = google_compute_instance_group.default.*.self_link
}

# this is idiomatic for how to have outputs that may have a count of zero
# https://www.terraform.io/upgrade-guides/0-11.html#referencing-attributes-from-resources-with-count-0
output "google_compute_region_backend_service_self_link" {
  value = element(
    concat(
      google_compute_region_backend_service.default.*.self_link,
      [""],
    ),
    0,
  )
}

output "google_compute_backend_service_self_link" {
  value = element(
    concat(google_compute_backend_service.default.*.self_link, [""]),
    0,
  )
}

output "google_compute_backend_service_iap_self_link" {
  value = element(
    concat(google_compute_backend_service.iap.*.self_link, [""]),
    0,
  )
}
